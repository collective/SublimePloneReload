SublimePloneReload
==================

plone.reload support for Sublime Text 2.


Usage:
------
The configuration is stored in `PloneReload.sublime-settings`.

Format:
```json
{
        "domain": "localhost",
        "port": "8080",
        "pw": "admin",
        "user": "admin",
        "enabled": true
}
```

plone.reload gets called "on save".


Installation
------------

Use `Package Control: Install Package` and search for `PloneReload`.

Or, if Package Control is not installed...

```
git clone https://github.com/maethu/SublimePloneReload.git
```

Into your ${path/to/sublime}/Packages

Info:
-----

Sublime Text 3 is currently not supported.

Author: https://github.com/maethu

Tracker: https://github.com/maethu/SublimePloneReload/issues

Source: https://github.com/maethu/SublimePloneReload
