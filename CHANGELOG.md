CHANGELOG
=========


1.1 (Unreleased)
----------------

- Add possibility to enable/disable the plugin.
- Handle unauthorized
- Better error msg.
  [mathias.leimgruber]


1.0 (2013-10-20)
----------------

- Init Version
- Support Sublime Text 2
  [mathias.leimgruber]